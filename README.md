## juniq - uniq(1) clone with additional options

This is a simple clone of uniq(1) with a couple of additional options:
* Can compare the first n bytes of each record.
* Can show only records that occur more than once.
* Can show records that occur only once.

If uniq(1) is available and you do not need to do something
very odd, you should use uniq(1) instead.  This is an
updated version of uniq I created for MS-DOS a very long
time ago.

Repositories:
* [gitlab repository](https://gitlab.com/jmcunx1/juniq) (this site)
* gemini://gem.sdf.org/jmccue/computer/repoinfo/juniq.gmi (mirror)
* gemini://sdf.org/jmccue/computer/repoinfo/juniq.gmi (mirror)

[Automake](https://en.wikipedia.org/wiki/Automake)
only confuses me, but this seems to be good enough for me.

To compile:
* If "DESTDIR" is not set, will install under /usr/local
* Execute ./build.sh to create a Makefile,
execute './build.sh --help' for options
* Then make and make install
* Works on Linux, BSD and AIX

To uninstall:
* make (see compile above)
* As root: 'make uninstall' from the source directory

This is licensed using the
[ISC Licence](https://en.wikipedia.org/wiki/ISC_license).
