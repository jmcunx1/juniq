.\"
.\" Copyright (c) 1997 ... 2024 2025
.\"     John McCue
.\"
.\" Permission to use, copy, modify, and distribute this software
.\" for any purpose with or without fee is hereby granted,
.\" provided that the above copyright notice and this permission
.\" notice appear in all copies.
.\"
.\" THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
.\" WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
.\" WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL
.\" THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR
.\" CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING
.\" FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
.\" CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
.\" OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS
.\" SOFTWARE.
.\"
.TH JUNIQ 1 "1997-09-21" "JMC" "User Commands"
.SH NAME
juniq - Prints Unique Values
.SH SYNOPSIS
juniq [OPTIONS] [FILE...]
.SH DESCRIPTION
Prints unique values (records) in a file.
Assumes the file(s) processed are sorted by the value being compared.
.PP
If no files (FILE) are specified on the command line or
if FILE has name '-', stdin (Standard Input) is used.
.SH OPTIONS
.TP
-C col
Optional, start compare at Column 'col'.
When enabled, records who's length is less than 'col'
will be skipped.
Default, compare at the start of each line.
.TP
-c
Optional, Prefix output with the count of items found.
.TP
-d
Optional, show only records that occur more than one time.
.TP
-e file
Optional, errors written to the file specified, defaults to stderr.
.TP
-f
Optional, force file create.
Create file even if the target file exists.
.TP
-H
Optional, print file name headings for each file processed.
Default, no headings.
.TP
-h
Show brief help and exit.
.TP
-o file
Optional, write output to the file specified, defaults to stdout.
.TP
-u
Optional, show only records that occur once.
.TP
-V
Output version information and exit.
.TP
-v
Optional, print I/O stats of files processed.
.TP
-w width
Optional, Width of the field to compare,
compare the first 'width' bytes of each record
to determine if unique.
Default, the full record is compared.
.SH DIAGNOSTICS
Processes one line at a time, assumes the file is a text file.
.PP
When using Column (-C), tabs are not expanded.
You may want to filter through expand(1) before
using 'juniq -C n'.
.PP
Best if Input files are sorted, but use cases exist
where you may not want to sort first.
For example when listing history from ksh(1),
you can bypass adjacent duplicates by doing this:
.nf
    $ fc -l -N 1000 | expand | juniq -C 9
.fi
.PP
If possible, use try uniq(1) instead.
But this utility may have a few options not available to uniq(1):
-d, -u, and/or -w (see above).
.SH BUGS
Do not know of any.
.SH ENVIRONMENT
none.
.SH ERROR-CODES
.nf
0 success
2 processing error or help/rev displayed
.fi
.SH SEE-ALSO
comm(1),
expand(1),
join(1),
ksh(1),
sort(1),
uniq(1)
