/*
 * Copyright (c) 1997 ... 2025 2026
 *     John McCue
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */
/*
 * juniq.h --  header
 */
#ifndef JUNIQ_H

#define JUNIQ_H    "3.5 2025/03/01"
#define PROG_NAME  "juniq"

#define INT_BUF_SIZE     2048
#define SWITCH_CHAR      '-'
#define FILE_NAME_STDIN  "-"
#define FILE_NAME_STDOUT "-"

#ifndef JMC_CHAR_NULL
#define JMC_CHAR_NULL  ((char) '\0')
#define JMC_UCHAR_NULL ((unsigned char) '\0' )
#endif

#ifndef NULL
#define NULL '\0'
#endif
#ifndef TRUE
#define TRUE 1
#endif
#ifndef FALSE
#define FALSE 0
#endif

/*** one of these should be OK for all OSs ***/
#ifdef MAXPATHLEN
#ifndef PATH_MAX
#define PATH_MAX MAXPATHLEN
#endif
#else
#ifdef PATH_MAX
#define MAXPATHLEN PATH_MAX
#endif
#endif

/*** arguments ***/
#define ARG_COL        'C'  /* Column to start with               */
#define ARG_COUNT      'c'  /* Prefix output with line number     */
#define ARG_DUPS       'd'  /* Duplicates                         */
#define ARG_ERR        'e'  /* Output Error File                  */
#define ARG_FORCE      'f'  /* force create files                 */
#define ARG_HEADINGS   'H'  /* print file headings                */
#define ARG_HELP       'h'  /* Show Help                          */
#define ARG_OUT        'o'  /* Output File                        */
#define ARG_UNIQ       'u'  /* Show unique only                   */
#define ARG_VERBOSE    'v'  /* Verbose                            */
#define ARG_VERSION    'V'  /* Show Version Information           */
#define ARG_WIDTH      'w'  /* Wheel Size                         */

/*** messages ***/
#define LIT_DESC               "Print Unique Records from a Sorted Text File"
#define LIT_C80                "::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::"
#define LIT_INFO_04            "Build: %s %s\n"
#define LIT_REV                "Revision"
#define MSG_ERR_00             "Try '%s %c%c' for more information\n"
#define MSG_ERR_02             "E02: Cannot open '%s' for write, processing aborted\n"
#define MSG_ERR_08             "E08: '%s' is an invalid value for %c%c, must be numeric\n"
#define MSG_ERR_17             "E17: inconsistent arguments for '%c%c' and '%c%c'\n"
#define MSG_ERR_20M            "E20: Cannot allocate %ld bytes of memory\n"
#define MSG_ERR_21M            "E21: Cannot allocate %ld bytes of memory for record %ld\n"
#define MSG_ERR_25             "E25: File %s cannot be created, already exists\n"
#define MSG_ERR_26             "E26: Argument %c%c specified more that once\n"
#define MSG_ERR_85             "E85: Value for %c%c greater than 0\n"
#define MSG_INFO_I03           "I03: Invalid  %9lu : File: %s\n"
#define MSG_INFO_I04           "I04: Reads    %9lu : Unique values Found %-lu : File: %s\n"
#define MSG_INFO_I05           "I05: Reads    %9lu : File: %s\n"
#define MSG_INFO_I06           "I06: Writes   %9lu : File: %s\n"
#define MSG_INFO_I52           "I52: Run Time %9.0f ms\n"
#define MSG_WARN_W02           "W02: Open Error Bypass File '%s' : %s\n"
#define USG_MSG_ARG_COL        "\t%c%c col\t\t: start line processing at column 'col'\n"
#define USG_MSG_ARG_COUNT_1    "\t%c%c\t\t: prefix output with number found\n"
#define USG_MSG_ARG_DUPS       "\t%c%c\t\t: Show lines that occur more than once.\n"
#define USG_MSG_ARG_ERR        "\t%c%c file\t\t: Write errors to file 'file', default stderr.\n"
#define USG_MSG_ARG_FORCE      "\t%c%c\t\t: force create of files when found.\n"
#define USG_MSG_ARG_HEADINGS   "\t%c%c\t\t: print file headings for each file processed.\n"
#define USG_MSG_ARG_HELP       "\t%c%c\t\t: Show brief help and exit.\n"
#define USG_MSG_ARG_OUT        "\t%c%c file\t\t: Write output to file 'file', default stdout.\n"
#define USG_MSG_ARG_UNIQ       "\t%c%c\t\t: Show only unique lines, lines with one occurrence.\n"
#define USG_MSG_ARG_VERBOSE_4  "\t%c%c\t\t: Show detail File Stats.\n"
#define USG_MSG_ARG_VERSION    "\t%c%c\t\t: Show revision information and exit.\n"
#define USG_MSG_ARG_WIDTH      "\t%c%c width\t: compare first 'width' characters on each line.\n"
#define USG_MSG_OPTIONS        "Options\n"
#define USG_MSG_USAGE          "usage:\t%s [OPTIONS] [FILES ...]\n"

/*** macros ***/
#define j2_bye_nl(x)    j2_bye_last((x), '\n')
#define j2_bye_ctlm(x)  j2_bye_last((x), '\r')

#endif /*  JUNIQ_H  */

/******************************* END: juniq.h ********************************/
