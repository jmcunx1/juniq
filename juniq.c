/*
 * Copyright (c) 1997 ... 2025 2026
 *     John McCue
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */
/*
* juniq.c -- simple unique value counter, assumes records are
*            sorted by value
*/
#ifdef ALLOW_LARGE_FILES
#define _FILE_OFFSET_BITS 64
#define __USE_LARGEFILE64
#endif

#ifndef _MSDOS
#include <sys/param.h>
#endif

#include <stdio.h>
#include <limits.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/stat.h>
#include <ctype.h>
#include <errno.h>
#include <time.h>

#ifdef UNIX
#include <libgen.h>
#include <unistd.h>
#endif
#ifdef MSDOSTYPE
#include <getopt.h>
#endif

#include "juniq.h"

/*** globals ***/
FILE *g_fp_err      = (FILE *) NULL;
char *g_fname_err   = (char *) NULL;

FILE *g_fp_out      = (FILE *) NULL;
char *g_fname_out   = (char *) NULL; 
size_t g_count_out  = (size_t) 0;

int g_arg_force         = (int) FALSE;
int g_arg_verbose       = (int) FALSE;
int g_arg_count         = (int) FALSE;
int g_arg_one           = (int) FALSE;
int g_arg_dups          = (int) FALSE;
int g_arg_headings      = (int) FALSE;
size_t g_col_start  = 0;
size_t g_max_length = 0;
size_t g_rec_num    = 0;

char *g_last_buf        = (char *) NULL;
char *g_buf             = (char *) NULL;
char *g_start_char      = (char *) NULL;
char *g_start_char_last = (char *) NULL;
size_t g_buf_size       = (size_t) INT_BUF_SIZE;
size_t g_buf_alloc      = (size_t) INT_BUF_SIZE;

/*
 * j2_rtw() -- removes trailing white space
 */
long int j2_rtw(char *buffer)

{
  char *last_non_space;
  char *b = buffer;

  if (buffer == (char *) NULL)
    return(0L); /* NULL pointer */

  last_non_space = buffer;

  for ( ; (*buffer) != JMC_CHAR_NULL; buffer++)
    {
      if ( ! isspace((int)(*buffer)) )
	last_non_space = buffer;
    }

  if ( ! isspace ((int) *last_non_space) )
    last_non_space++;

  (*last_non_space) = JMC_CHAR_NULL;

  return((long int) strlen(b));

} /* j2_rtw() */

/*
 * j2_bye_last() -- When the last byte = x, replaces it with NULL 
 *                  and returns string length
 */
long int j2_bye_last(char *x, char lastc)

{
  int i;
  
  if ( x == (char *) NULL )
    return(0L);
  if (strlen(x) < 1)
    return(0L);
  
  i = strlen(x) - (size_t) 1;
  
  if (x[i] == lastc)
    x[i] = JMC_CHAR_NULL;
  
  return((long int) strlen(x));
  
} /* j2_bye_last() */

/*
 * j2_f_exist() -- determines if a file exists
 */
int j2_f_exist(char *file_name)

{
  if (file_name == (char *) NULL)
    return((int) FALSE);

#ifdef _MSDOS
  if (access(file_name, 00) == -1)
    return (FALSE);
  else
    return (TRUE);
#else
  struct stat file_info;
  if (stat(file_name, &file_info) == 0)
    return (TRUE);
  else
    return (FALSE);
#endif

} /* j2_f_exist() */

/*
 * j2_is_numr() -- determines if all characters are numeric
 */
int j2_is_numr(char *s)

{
  if (s == (char *) NULL)
    return((int) FALSE);
  
  for ( ; (*s) != JMC_CHAR_NULL; s++)
    {
      if ( ! isdigit((int)(*s)) )
	return(FALSE);
    }
  
  return(TRUE);

} /* j2_is_numr() */

/*
 * open_out() -- if allowed, open the file
 */
void open_out(FILE **fp, char *fname, int force)

{
  if (fname == (char *) NULL)
    return;

  if (force == (int) FALSE)
    {
      if ( j2_f_exist(fname) )
	{
	  fprintf(stderr, MSG_ERR_00, PROG_NAME, SWITCH_CHAR, ARG_HELP);
	  fprintf(stderr, MSG_ERR_25, fname);
	  exit(EXIT_FAILURE);
	}
    }


  (*fp) = fopen(fname, "w");
  if ((*fp) == (FILE *) NULL)
    {
      fprintf(stderr, MSG_ERR_02, fname);
      fprintf(stderr, "\t%s\n", strerror(errno));
      exit(EXIT_FAILURE);
    }

} /* open_out() */

/*
 * show_rev() -- displays Version Information
 */
void show_rev(FILE *fp)

{

  fprintf(fp,"%s: %s\n", PROG_NAME, LIT_DESC);

  fprintf(fp,"\t%s %s: %s - ", LIT_REV, PROG_NAME, JUNIQ_H);
  fprintf(fp, LIT_INFO_04, __DATE__, __TIME__);

#ifdef OSTYPE
  fprintf(fp,"\t%s\n", OSTYPE);
#endif
#ifdef PRETTY_NAME
  fprintf(fp,"\t%s\n",PRETTY_NAME);
#endif

  exit(EXIT_FAILURE);

} /* show_rev() */

/*
 * show_help() -- displays show_help info and exits
 */
void show_help(FILE *fp)

{

  fprintf(fp, USG_MSG_USAGE, PROG_NAME);
  fprintf(fp, "\t%s\n", LIT_DESC);
  fprintf(fp, USG_MSG_OPTIONS);
  fprintf(fp, USG_MSG_ARG_COL,       SWITCH_CHAR, ARG_COL);
  fprintf(fp, USG_MSG_ARG_COUNT_1,   SWITCH_CHAR, ARG_COUNT);
  fprintf(fp, USG_MSG_ARG_DUPS,      SWITCH_CHAR, ARG_DUPS);
  fprintf(fp, USG_MSG_ARG_ERR,       SWITCH_CHAR, ARG_ERR);
  fprintf(fp, USG_MSG_ARG_FORCE,     SWITCH_CHAR, ARG_FORCE);
  fprintf(fp, USG_MSG_ARG_HEADINGS,  SWITCH_CHAR, ARG_HEADINGS);
  fprintf(fp, USG_MSG_ARG_HELP,      SWITCH_CHAR, ARG_HELP);
  fprintf(fp, USG_MSG_ARG_OUT,       SWITCH_CHAR, ARG_OUT);
  fprintf(fp, USG_MSG_ARG_UNIQ,      SWITCH_CHAR, ARG_UNIQ);
  fprintf(fp, USG_MSG_ARG_VERSION,   SWITCH_CHAR, ARG_VERSION);
  fprintf(fp, USG_MSG_ARG_VERBOSE_4, SWITCH_CHAR, ARG_VERBOSE);
  fprintf(fp, USG_MSG_ARG_WIDTH,     SWITCH_CHAR, ARG_WIDTH);

  exit(EXIT_FAILURE);

} /* show_help() */

/*
 * get_args() -- load arguments
 */
void get_args(int argc, char **argv)

{
  char ckarg[80];
  int opt;

  memset(ckarg, 0, 80);
  snprintf(ckarg, 79, "%c:%c:%c:%c:%c%c%c%c%c%c%c%c", 
                 ARG_ERR,  ARG_OUT,   ARG_WIDTH,   ARG_COL,
                 ARG_HELP, ARG_FORCE, ARG_VERSION, ARG_COUNT,
                 ARG_DUPS, ARG_UNIQ,  ARG_VERBOSE, ARG_HEADINGS);

  while ((opt = getopt(argc, argv, ckarg)) != -1)
    {
      switch (opt)
	{
	  case ARG_HELP:
	    show_help(stderr);
	    break;
	  case ARG_VERSION:
	    show_rev(stderr);
	    break;
	  case ARG_HEADINGS:
	    g_arg_headings = (int) TRUE;
	    break;
	  case ARG_FORCE:
	    g_arg_force = (int) TRUE;
	    break;
	  case ARG_COUNT:
	    g_arg_count = (int) TRUE;
	    break;
	  case ARG_DUPS:
	    g_arg_dups = (int) TRUE;
	    break;
	  case ARG_UNIQ:
	    g_arg_one = (int) TRUE;
	    break;
	  case ARG_COL:
	    if ( ! j2_is_numr(optarg) )
	      {
		fprintf(stderr, MSG_ERR_08, optarg,    SWITCH_CHAR, ARG_COL);
		fprintf(stderr, MSG_ERR_00, PROG_NAME, SWITCH_CHAR, ARG_HELP);
		exit(EXIT_FAILURE);
	      }
	    g_col_start = (size_t) atol(optarg);
	    if (g_col_start == 0)
	      {
		fprintf(stderr, MSG_ERR_85, SWITCH_CHAR, ARG_COL);
		fprintf(stderr, MSG_ERR_00, PROG_NAME, SWITCH_CHAR, ARG_HELP);
		exit(EXIT_FAILURE);
	      }
	    g_col_start--;
	    break;
	  case ARG_WIDTH:
	    if ( ! j2_is_numr(optarg) )
	      {
		fprintf(stderr, MSG_ERR_08, optarg,    SWITCH_CHAR, ARG_WIDTH);
		fprintf(stderr, MSG_ERR_00, PROG_NAME, SWITCH_CHAR, ARG_HELP);
		exit(EXIT_FAILURE);
	      }
	    g_max_length = (size_t) atol(optarg);
	    break;
	  case ARG_VERBOSE:
	    g_arg_verbose = (int) TRUE;
	    break;
	  case ARG_OUT:
	    if (g_fname_out != (char *) NULL)
	      {
		fprintf(stderr, MSG_ERR_26, SWITCH_CHAR, ARG_OUT);
		fprintf(stderr, MSG_ERR_00, PROG_NAME, SWITCH_CHAR, ARG_HELP);
		exit(EXIT_FAILURE);
	      }
	    g_fname_out = strdup(optarg);
	    break;
	  case ARG_ERR:
	    if (g_fname_err != (char *) NULL)
	      {
		fprintf(stderr, MSG_ERR_26, SWITCH_CHAR, ARG_ERR);
		fprintf(stderr, MSG_ERR_00, PROG_NAME, SWITCH_CHAR, ARG_HELP);
		exit(EXIT_FAILURE);
	      }
	    g_fname_err = strdup(optarg);
	    break;
	  default:
	    fprintf(stderr, MSG_ERR_00, PROG_NAME, SWITCH_CHAR, ARG_HELP);
	    exit(EXIT_FAILURE);
	    break;
	}
    }

  /*** open output ***/
  open_out(&g_fp_err, g_fname_err, g_arg_force);
  open_out(&g_fp_out, g_fname_out, g_arg_force);

  /*** show help/rev and exit ? ***/

  if (g_arg_one && g_arg_dups)
    {
      fprintf(g_fp_err, MSG_ERR_17, SWITCH_CHAR, ARG_DUPS, SWITCH_CHAR, ARG_UNIQ);
      fprintf(g_fp_err, MSG_ERR_00, PROG_NAME, SWITCH_CHAR, ARG_HELP);
      exit(EXIT_FAILURE);
    }

} /* get_args() */

/*
 * init()
 */
void init(int argc, char **argv)
{
  g_fp_err = stderr;
  g_fp_out = stdout;

  get_args(argc, argv);

  /*** allocate initial memory ***/
  g_buf      = calloc(sizeof(char), g_buf_size);
  if (g_buf == (char *) NULL)
    {
      fprintf(g_fp_err, MSG_ERR_20M, g_buf_size);
      fprintf(g_fp_err, MSG_ERR_00, PROG_NAME, SWITCH_CHAR, ARG_HELP);
      exit(EXIT_FAILURE);
    }
  g_last_buf = calloc(sizeof(char), g_buf_size);
  if (g_last_buf == (char *) NULL)
    {
      fprintf(g_fp_err, MSG_ERR_20M, g_buf_size);
      fprintf(g_fp_err, MSG_ERR_00, PROG_NAME, SWITCH_CHAR, ARG_HELP);
      exit(EXIT_FAILURE);
    }

} /* init() */

/*
 * show_rec()
 */
void show_rec(char *val, size_t found)
{

  if (g_arg_one == TRUE)
    {
      if (found != 1)
	return;
    }
  if (g_arg_dups == TRUE)
    {
      if (found == 1)
	return;
    }

  if (g_arg_count == TRUE)
    fprintf(g_fp_out, "%20ld %s\n", (long int) found, val);
  else
    fprintf(g_fp_out, "%s\n", val);
  g_count_out++;

} /* show_rec() */

/*
 * reset_mem() -- re-allocate memory if buffer size changed
 */
void reset_mem(void)
{

  if (g_buf_size <= g_buf_alloc)
    return;

  g_buf_alloc = g_buf_size;
  if (realloc(g_last_buf, (sizeof(char) * g_buf_alloc)) == (char *) NULL)
    {
      fprintf(g_fp_err, MSG_ERR_21M, g_buf_alloc, g_rec_num);
      fprintf(g_fp_err, MSG_ERR_00, PROG_NAME, SWITCH_CHAR, ARG_HELP);
    }

} /* reset_mem() */

/*
 * process_file() -- process a file
 */
void process_file(char *fname)
{
  static size_t number_found = 0;
  size_t uniq_found = 0;
  size_t invalid_found = 0;
  int dif_results = 0;
  FILE *fp;

  if (g_arg_headings == TRUE)
    {
      if (g_last_buf != (char *) NULL)     /* clear because we want */
	memset(g_last_buf, 0, g_buf_size); /* to use headings       */
      fprintf(g_fp_out, "%s\n%s\n%s\n", LIT_C80, (fname == (char *) NULL ? "stdin" : fname), LIT_C80);
      g_count_out += 3;
    }

  if (fname != (char *) NULL)
    {
      if (strcmp(fname, FILE_NAME_STDIN) == 0)
	fp = stdin;
      else
	{
	  fp = fopen(fname, "r");
	  if (fp == (FILE *) NULL)
	    {
	      fprintf(g_fp_err, MSG_WARN_W02, fname, strerror(errno));
	      return;
	    }
	}
    }
  else
    fp = stdin;

  while (getline(&g_buf, &g_buf_size, fp) > (ssize_t) -1)
    {
      g_rec_num++;
      reset_mem();
      j2_bye_nl(g_buf);
      j2_bye_ctlm(g_buf);
      if (g_col_start > 0)
	{
	  if (strlen(g_buf) < g_col_start) 
	    {
	      invalid_found++;
	      continue;
	    }
	}
      if (strlen(g_last_buf) < 1)
	{
	  number_found = (size_t) 1;
	  g_last_buf = strncpy(g_last_buf, g_buf, g_buf_size);
	  continue;
	}
      if (g_col_start == 0)
	{
	  if (g_max_length == 0)
	    dif_results = strcmp(g_buf, g_last_buf);
	  else
	    dif_results = strncmp(g_buf, g_last_buf, g_max_length);
	}
      else
	{
	  g_start_char      = &(g_buf[g_col_start]);
	  g_start_char_last = &(g_last_buf[g_col_start]);
	  if (g_max_length == 0)
	    dif_results = strcmp(g_start_char, g_start_char_last);
	  else
	    dif_results = strncmp(g_start_char, g_start_char_last, g_max_length);
	}
      if (dif_results == 0)
	number_found++;
      else
	{
	  uniq_found++;
	  show_rec(g_last_buf, number_found);
	  number_found = (size_t) 1;
	  memset(g_last_buf, 0, g_buf_size);
	  strncpy(g_last_buf, g_buf, g_buf_size);
	}
    }

  if (number_found > 0)
    {
      uniq_found++;
      show_rec(g_last_buf, number_found);
    }

  if (fname != (char *) NULL)
    fclose(fp);

  if (g_arg_verbose == TRUE)
    {
      if (g_arg_count == TRUE)
	fprintf(g_fp_err, MSG_INFO_I04, (unsigned long int) g_rec_num, (unsigned long int) uniq_found, (fname == (char *) NULL ? "stdin" : fname));
      else
	fprintf(g_fp_err, MSG_INFO_I05, (unsigned long int) g_rec_num, (fname == (char *) NULL ? "stdin" : fname));
      if (invalid_found > 0)
	fprintf(g_fp_err, MSG_INFO_I03, (unsigned long int) invalid_found, (fname == (char *) NULL ? "stdin" : fname));
    }

  g_rec_num = (size_t) 0;

} /* process_file() */

/*
 * main()
 */
int main(int argc, char **argv)
{
  int i;
  clock_t tstart = clock();

  init(argc, argv);

  /*** process data ***/
  for (i = optind; i < argc; i++)
    process_file(argv[i]);
  if (i == optind)
    process_file(FILE_NAME_STDIN);

  if (g_arg_verbose == TRUE)
    {
      fprintf(g_fp_err, MSG_INFO_I06, (unsigned long int) g_count_out, (g_fname_out == (char *) NULL ? "stdout" : g_fname_out));
    }

  /*** DONE ***/
  if (g_buf != (char *) NULL)
    free(g_buf);
  if (g_last_buf != (char *) NULL)
    free(g_last_buf);
  if (g_fname_out != (char *) NULL)
    {
      fclose(g_fp_out);
      free(g_fname_out);
    }

  if (g_arg_verbose == TRUE)
    {
      fprintf(g_fp_err, MSG_INFO_I52,
         (double)(clock() - tstart) * 1000 / (double) CLOCKS_PER_SEC);
    }

  if (g_fname_err != (char *) NULL)
    {
      fclose(g_fp_err);
      free(g_fname_err);
    }

  exit(EXIT_SUCCESS);

} /* main() */
